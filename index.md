---
layout: home
---

<div id="lightgallery">
{% for image in site.static_files %}
  {% if image.path contains '/photos/' %}
    <a href="{{ image.path | relative_url }}">
      <img src="{{ image.path | relative_url }}" alt="{{ image.basename }}" style="height: 240px;">
    </a>
  {% endif %}
{% endfor %}
</div>

<script type="text/javascript">
    lightGallery(document.getElementById('lightgallery'), {
        plugins: [lgZoom, lgThumbnail, lgFullscreen],
        speed: 500,
        thumbnail: true,
    });
</script>