## About this site

This single-page Jekyll site is in progress big-time, especially the CSS. 

It uses Liquid to loop through a photo folder and build poorly formatted, non-responsive thumbnails that take forever to load and then open in [lightGallery](https://www.lightgalleryjs.com/) while your computer explodes. 

I'm using the [default Jekyll theme](https://github.com/jekyll/minima), so the only relevant files are:

* **index.md** - Contains the Liquid loop and lightGallery script. Points to a layout page in its YAML header.
* **home.html** - Lives quietly in `~/_layouts` and does nothing but point to its friend, `base.html`. 
* **base.html** - Loads the main lightGallery script, its plugins, and some CSS. I linked to the Bootstrap CSS on their CDN since I might try to do [this](https://codepen.io/sachinchoolur/pen/poebzpV), but haven't implemented it yet. Bootstrap seems to use [lots of this](https://miromannino.github.io/Justified-Gallery/), so maybe I'll just go straight there. 

## Known issues

Now that I've fried my brain [by attempting this](https://jimmyxiao.me/guides/technical/2019/07/28/folder-based-jekyll-lightgallery) and realizing it's incredibly hacky, I only need to:

1. Figure out why the site is _dog slow_, at least when run locally, but probably anywhere. 
2. Poke at some CSS until a masonry layout appears.

For the first item, I'm guessing the site is slow since it's trying to load 100 full-size photos at once, but if my code is doing something else horrible, I wouldn't know it. 

There may be some info in the [lightGallery docs](https://www.lightgalleryjs.com/docs/getting-started/), but I sure wouldn't count on it. I also [started a discussion](https://github.com/sachinchoolur/lightGallery/discussions/1612), but I'll only end up replying to myself there, since that guy is objectively not going to bother. 

## Dependencies

If you actually want to run this thing, question your life choices. Once that reflection is complete, go get:

1. Ruby 2.5.x and RubyGems
2. GCC and Make
3. Jekyll (natch)

More information is in the [Jekyll docs](https://jekyllrb.com/docs/). 